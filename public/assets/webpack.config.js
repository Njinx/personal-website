const path = require('path');

module.exports = {
    entry: './ts/main.ts',
    mode: 'production',
    devtool: 'inline-source-map',
    output: {
        path: path.resolve(__dirname, 'js/'),
        filename: 'main.js',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    }
};