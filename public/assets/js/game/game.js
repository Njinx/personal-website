import Player from './player.js';
var vpWidth = window.innerWidth;
var vpHeight = window.innerHeight;
var vpCenterX = vpWidth / 2;
var vpCenterY = vpHeight / 2;
var bgColor = '#1e1e1e';
var fgColor = '#2eec71';
var logo = document.getElementById('logo');
var Game = (function () {
    function Game(player) {
        this.player = player;
        this.canvas = document.createElement('canvas');
        this.canvas.id = 'gameCanvas';
        this.canvas.style.position = 'absolute';
        this.canvas.style.top = '0';
        this.canvas.style.left = '0';
        this.canvas.style.width = '100%';
        this.canvas.style.height = '100%';
        this.canvas.style.zIndex = '9999';
        if (this.canvas.getContext('2d') === null) {
            this.ctx = new CanvasRenderingContext2D();
            return;
        }
        else {
            this.ctx = this.canvas.getContext('2d');
        }
        document.body.appendChild(this.canvas);
        var i = 0;
        var drawTimer = setInterval(function () {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.beginPath();
            this.ctx.arc(this.canvas.width / 2, this.canvas.height / 2, i * 2, 0, Math.PI * 2, false);
            this.ctx.fillStyle = bgColor;
            this.ctx.fill();
            i++;
            if (i >= this.canvas.width) {
                clearInterval(drawTimer);
            }
        }, 1);
    }
    Game.prototype.render = function () {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.beginPath();
        this.ctx.rect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.closePath();
        this.ctx.fillStyle = bgColor;
        this.ctx.fill();
        var groundY = this.canvas.height - 10;
        this.ctx.beginPath();
        this.ctx.moveTo(0, groundY);
        this.ctx.lineTo(this.canvas.width, groundY);
        this.ctx.closePath();
        this.ctx.fillStyle = fgColor;
        this.ctx.fill();
        this.ctx.drawImage(this.player.sprite, 0, 0);
    };
    return Game;
}());
export function run() {
    if (!!document.createElement('canvas').getContext && logo !== null) {
        logo.addEventListener('click', function () {
            var player = new Player();
            var game = new Game(player);
            if (game !== null) {
                game.render();
            }
        });
    }
}
//# sourceMappingURL=game.js.map