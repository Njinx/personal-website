var SPRITE_PATH = './assets/ts/game/character_sprites/';
var Player = (function () {
    function Player() {
        this.state = PlayerState.STATIONARY;
        this.direction = PlayerDirection.NONE;
        this.pos = {
            x: 0,
            y: 0
        };
        this.sprite = new Image();
        this.sprite.src = SPRITE_PATH + 'stationary.png';
    }
    Player.prototype.updateSpriteImage = function () {
        var stateString;
        var directionString;
        switch (this.state) {
            case PlayerState.STATIONARY:
                stateString = 'stationary';
                break;
            case PlayerState.ACTIVE:
                stateString = 'active';
                break;
        }
        switch (this.direction) {
            case PlayerDirection.NONE:
                directionString = '';
                break;
            case PlayerDirection.LEFT:
                directionString = '_left';
                break;
            case PlayerDirection.RIGHT:
                directionString = '_right';
                break;
            case PlayerDirection.UP:
                directionString = '_up';
                break;
        }
        var fileName = stateString + directionString + '.png';
        this.sprite.src = SPRITE_PATH + fileName;
    };
    return Player;
}());
export default Player;
var PlayerState;
(function (PlayerState) {
    PlayerState[PlayerState["STATIONARY"] = 0] = "STATIONARY";
    PlayerState[PlayerState["ACTIVE"] = 1] = "ACTIVE";
})(PlayerState || (PlayerState = {}));
var PlayerDirection;
(function (PlayerDirection) {
    PlayerDirection[PlayerDirection["NONE"] = 0] = "NONE";
    PlayerDirection[PlayerDirection["LEFT"] = 1] = "LEFT";
    PlayerDirection[PlayerDirection["RIGHT"] = 2] = "RIGHT";
    PlayerDirection[PlayerDirection["UP"] = 3] = "UP";
})(PlayerDirection || (PlayerDirection = {}));
//# sourceMappingURL=player.js.map