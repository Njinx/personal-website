const SPRITE_PATH: string = './assets/ts/game/character_sprites/';

export default class Player {
    state: PlayerState;
    direction: PlayerDirection;
    pos: {x: number, y: number};
    sprite: HTMLImageElement;

    constructor() {
        this.state = PlayerState.STATIONARY;
        this.direction = PlayerDirection.NONE;
        this.pos = {
            x: 0,
            y: 0,
        };
        this.sprite = new Image();
        this.sprite.src = SPRITE_PATH + 'stationary.png';
    }

    private updateSpriteImage() {
        let stateString: string;
        let directionString: string;
        
        switch (this.state) {
            case PlayerState.STATIONARY:
                stateString = 'stationary';
                break;
            case PlayerState.ACTIVE:
                stateString = 'active';
                break;
        }

        switch (this.direction) {
            case PlayerDirection.NONE:
                directionString = '';
                break;
            case PlayerDirection.LEFT:
                directionString = '_left';
                break;
            case PlayerDirection.RIGHT:
                directionString = '_right';
                break;
            case PlayerDirection.UP:
                directionString = '_up';
                break;
        }

        let fileName = stateString + directionString + '.png';
        this.sprite.src = SPRITE_PATH + fileName;
    }
}

enum PlayerState {
    STATIONARY,
    ACTIVE,
}

enum PlayerDirection {
    NONE,
    LEFT,
    RIGHT,
    UP,
}