import Player from './player.js';

let vpWidth = window.innerWidth;
let vpHeight = window.innerHeight;
let vpCenterX = vpWidth / 2;
let vpCenterY = vpHeight / 2;

const bgColor = '#1e1e1e';
const fgColor = '#2eec71';

const logo = document.getElementById('logo');

class Game {
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
    player: Player;

    constructor(player: Player) {
        this.player = player;

        this.canvas = document.createElement('canvas');
        this.canvas.id = 'gameCanvas';
        this.canvas.style.position = 'absolute';
        this.canvas.style.top = '0';
        this.canvas.style.left = '0';
        this.canvas.style.width = '100%';
        this.canvas.style.height = '100%';
        this.canvas.style.zIndex = '9999';

        if (this.canvas.getContext('2d') === null) {
            this.ctx = new CanvasRenderingContext2D();
            return;
        } else {
            this.ctx = this.canvas.getContext('2d') as CanvasRenderingContext2D;
        }

        document.body.appendChild(this.canvas);

        let i = 0;
        const drawTimer = setInterval(function(this: Game) {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

            this.ctx.beginPath();
            this.ctx.arc(this.canvas.width/2, this.canvas.height/2, i*2, 0, Math.PI*2, false);
            this.ctx.fillStyle = bgColor;
            this.ctx.fill();

            i++;

            if (i >= this.canvas.width) {
                clearInterval(drawTimer);
            }
        }, 1);
    }

    render() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    
        // Draw background
        this.ctx.beginPath();
        this.ctx.rect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.closePath();
        this.ctx.fillStyle = bgColor;
        this.ctx.fill();
    

        // Draw ground
        let groundY = this.canvas.height - 10;
        this.ctx.beginPath();
        this.ctx.moveTo(0, groundY);
        this.ctx.lineTo(this.canvas.width, groundY);
        this.ctx.closePath();
        this.ctx.fillStyle = fgColor;
        this.ctx.fill();

        // Draw player
        this.ctx.drawImage(this.player.sprite, 0, 0);
    }
}

export function run() {
    if (!!document.createElement('canvas').getContext && logo !== null) {
        logo.addEventListener('click', function() {
            let player = new Player();
            let game = new Game(player);
            if (game !== null) {
                game.render();
            }
        });
    }
}
